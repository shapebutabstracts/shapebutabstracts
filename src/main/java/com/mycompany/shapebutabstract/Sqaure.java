package com.mycompany.shapebutabstract;

public class Sqaure extends Shape {

    private double side;

    public Sqaure(double side) {
        super("Sqaure");
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double calArea() {
        return side * side;
    }

    @Override
    public String toString() {
        return "Sqaure { side = " + side + " }";
    }
}
