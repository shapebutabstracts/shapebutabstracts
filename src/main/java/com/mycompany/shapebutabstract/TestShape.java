package com.mycompany.shapebutabstract;

public class TestShape {

    public static void main(String[] args) {
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);

        Rectangle rec1 = new Rectangle(3, 2);
        Rectangle rec2 = new Rectangle(4, 3);
        System.out.println(rec1);
        System.out.println(rec2);

        Sqaure sq1 = new Sqaure(4.0);
        Sqaure sq2 = new Sqaure(2.0);
        System.out.println(sq1);
        System.out.println(sq2);

        System.out.println("------------------------------");

        Shape[] shapes = {c1, c2, c3, rec1, rec2, sq1, sq2};
        for (int i = 0; i < shapes.length; i++) {
            System.out.printf("%s area : %.2f\n", shapes[i].getName(), shapes[i].calArea());
        }
    }
}
