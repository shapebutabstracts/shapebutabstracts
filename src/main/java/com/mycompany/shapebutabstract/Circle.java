package com.mycompany.shapebutabstract;

public class Circle extends Shape {

    private double r;
    private static final double PI = 22.0 / 7;

    public Circle(Double r) {
        super("Circle");
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double calArea() {
        return PI * r * r;
    }

    @Override
    public String toString() {
        return "Circle{ r = " + r + " }";
    }
}
